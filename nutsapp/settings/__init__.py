import os
from django.core.exceptions import ImproperlyConfigured

nutsapp_mode = os.environ.get('MODE')


# check if the app mode is scpecified
if not nutsapp_mode:
    raise ImproperlyConfigured("No mode specified for project")

# check if mode is in dev and run it with development settings and modules
if nutsapp_mode == 'DEV' or nutsapp_mode == 'dev':
    from nutsapp.settings.dev import *

# check if mode is in prod and run it with the production settings and modules
elif nutsapp_mode == 'PROD' or nutsapp_mode == 'prod':
    from nutsapp.settings.prod import *

# check if mode is not dev or prod
else:
    raise ImproperlyConfigured(
        "Please specify correct mode in your env file MODE=DEV||prod or MODE=PROD||prod")
