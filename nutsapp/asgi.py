"""
ASGI config for backendProject project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/4.1/howto/deployment/asgi/
"""

import os

from django.core.asgi import get_asgi_application
from channels.routing import ProtocolTypeRouter
from django.core.asgi import get_asgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'nutsapp.settings')

nutsapp_asgi_app = get_asgi_application()

application = ProtocolTypeRouter({
    "http": nutsapp_asgi_app,
})
